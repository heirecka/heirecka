# Copyright 2010 Bernhard Frauendienst
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam package=01
myexparam release=
myexparam file=02
myexparam suffix=tar.gz

MY_PNV="${PNV//_/}"
MY_FILENAME="${MY_PNV}.$(exparam suffix)"

require option-renames [ renames=[ 'gtk providers:gtk2' ] ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="A multi-platform helper library for networking and security applications and libraries"
HOMEPAGE="https://www.aquamaniac.de/aqbanking"
DOWNLOADS="https://www.aquamaniac.de/sites/download/download.php?package=$(exparam package)&release=$(exparam release)&file=$(exparam file)&dummy=${MY_FILENAME} -> ${MY_FILENAME}"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    debug doc qt5
    ( providers: gtk2 gtk3 )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/gnutls[>=2.9.8]
        dev-libs/libgcrypt[>=1.2.0]
        dev-libs/libgpg-error[>=1.9]
        providers:gtk2? ( x11-libs/gtk+:2[>=2.17.5] )
        providers:gtk3? ( x11-libs/gtk+:3[>=3.14.0] )
        (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        ) [[ note = [ SSL Support for gct-tool ] ]]
        qt5? ( x11-libs/qtbase:5 )
"

WORK=${WORKBASE}/${MY_PNV}

gwenhywfar_src_configure() {
    local guis=() qtoptions=()
    optionq providers:gtk2 && guis+=( gtk2 )
    optionq providers:gtk3 && guis+=( gtk3 )
    if optionq qt5 ; then
        guis+=( qt5 )
        qtoptions+=(
            --with-qt5-qmake="/usr/$(exhost --target)/lib/qt5/bin/qmake"
            --with-qt5-moc="/usr/$(exhost --target)/lib/qt5/bin/moc"
            --with-qt5-uic="/usr/$(exhost --target)/lib/qt5/bin/uic"
        )
    fi

    econf \
        "${qtoptions[@]}" \
        --enable-debug=$(option debug yes no) \
        --enable-system-certs \
        --disable-network-checks \
        --with-docpath=/usr/share/doc/${PNVR}/apidoc \
        --with-guis="${guis[*]}"
}

gwenhywfar_src_compile() {
    default

    option doc && emake srcdoc
}

gwenhywfar_src_install() {
    default

    option doc && emake DESTDIR="${IMAGE}" install-srcdoc
}

