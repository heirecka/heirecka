# Copyright 2010 Yury G. Kudryashov
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/$(ever range 1-2)" ] kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="RSIBreak helps you to prevent Repetitive Strain Injury (RSI)"

DESCRIPTION="
Repetitive Strain Injury (RSI) is an illness which can occur as a result of continuous work
with a mouse and keyboard. The risk of suffering injury increases the longer users work without
breaks. RSIBreak simply offers reminders to take a break now and then.
"

HOMEPAGE+=" https://userbase.kde.org/RSIBreak"
LICENCES="FDL-1.2 GPL-2"
SLOT="0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
    build+run:
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcrash:5
        kde-frameworks/kdbusaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kidletime:5
        kde-frameworks/knotifications:5
        kde-frameworks/knotifyconfig:5
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/kxmlgui:5
        x11-libs/qtbase:5[>=5.3.0]
"

# 1 of 2 tests needs a running X server, the other is just an appstream test
# which is actually skipped for us anyway.
RESTRICT="test"

